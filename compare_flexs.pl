#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use Getopt::Long;
use Encode qw(decode);
binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

################################
##### COMMAND LINE OPTIONS #####
################################

my $new; # New version of the list of felxed forms
my $old; # Old version of the list of flexed forms
my $language; # Optional: identifier of the language

my $STORE = {}; # Forms found in both files are stored in here

GetOptions ("new=s" => \$new,
	    "old=s" => \$old,
	    "language=s" => \$language) or die("Error in command line arguments\n");

die "No files provided" unless $new && $old;

#########################
##### CREATE MODELS #####
#########################

# CREATING REPOSITORY WITH THE NEW FORMS

open my $new_fh, '<:encoding(UTF-8)', $new 
    || die "Models file $new not found";
say STDERR "Reading new flexions:";
parse_and_store($_,'new') while <$new_fh>;
close $new_fh;

# CREATING REPOSITORY WITH THE OLD FORMS

open my $old_fh, '<:encoding(UTF-8)', $old 
    || die "Models file $old not found";
say STDERR "Reading old flexions:";
parse_and_store($_,'old') while <$old_fh>;
close $old_fh;

# FINDING DISCREPANCIES AND WRITING FILES
# FOR DIFFS

say STDERR "Finding discrepancies:";
find_discrepancies();

###############
## FUNCTIONS ##
###############

sub parse_and_store
{
    my ($line,$old_or_new) = @_;
    chomp $line;
    my ($form,$lemma,$analysis) = $line =~ m/^([^\t]+)\t([^\t]+)\t(.+)$/;
    defined || return for ($form,$lemma,$analysis); 

    return unless $analysis =~ /^(verb|adjective|noun)\t/;

    print STDERR "$form                    \r";    
    # Stores if it has been found in the old or the new version of flexion
    $STORE->{$lemma}->{$form}->{$analysis}->{$old_or_new} = 1; 
}

my $MISSING_LEMMAS = {}; # Stores the lemmas that only appear in one flexion (old or new)


# For each lemma found in the input files (stored in $STORE)
# discrepancies are identified if some forms of one version don't appear in the other one
# If that situation is found, a message is printed in the corresponding output

sub find_discrepancies
{
    # File to write lemmas that appear only in one version
    open my $fh, '>:encoding(UTF-8)', './_lack_of_lemmas_.txt' 
    || die "Unable to open ./_lack_of_lemmas_.txt";

    for my $lemma (sort keys %$STORE)
    {
	print STDERR "$lemma                    \r";    
	my ($in_old,$in_new,$no_in_old,$no_in_new) = (0,0);
	for my $form (sort keys %{$STORE->{$lemma}})
	{
	    while (my ($analysis,$where) = each %{$STORE->{$lemma}->{$form}})
	    {
		if ($where->{old}) { $in_old++; } else { $no_in_old = 1; }
		if ($where->{new}) { $in_new++; } else { $no_in_new = 1; }
	    }
	}
		
	print_diff_for_lemma($lemma)
	    if (($in_old > 0 && $in_new > 0 && $in_old ne $in_new) || 	# Different number of forms
		(($no_in_old || $no_in_new) && $in_old eq $in_new));    # The same number of forms, but they are not equal 

	say $fh "$lemma\t$in_old\t$in_new" if 
	    (($in_old eq 0 || $in_new eq 0) && $in_old ne $in_new);     # Lack of lemma in one version 
    }
    close $fh;
}

sub print_diff_for_lemma
{
    my $lemma = shift;

    mkdir './diffsOld';
    mkdir './diffsNew';

    open my $old_fh, '>:encoding(UTF-8)', "./diffsOld/$lemma" 
    || die "Unable to open ./diffsOld/$lemma";

    open my $new_fh, '>:encoding(UTF-8)', "./diffsNew/$lemma" 
    || die "Unable to open ./diffsNew/$lemma.new";

    for my $form (sort keys %{$STORE->{$lemma}})
    {
	while (my ($analysis,$where) = each %{$STORE->{$lemma}->{$form}})
	{
	    say $old_fh "$form\t$lemma\t$analysis" if $where->{old};
	    say $new_fh "$form\t$lemma\t$analysis" if $where->{new};
	}
    }

    close $old_fh; close $new_fh;
}

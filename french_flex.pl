#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use Getopt::Long;

# This script takes a Model Rules File for German
# and a list of Lemmas for German.

# It parses the Model Rules and store a bunch of
# models with rules to flex.

# With that models stored, the file of lemmas is read
# For every lemma, according the stored model correspondig
# that lemma, the flexed forms are generated and printed in STDOUT. 
# At the end of the file appears the README that describes the files
# that must be provided to this script

# At the end of the file appears the README that describes the files
# that must be provided to this script

##############
### README ###
##############

# Perl 14.04 needed
# Module Moo needed. In order to install it:
# > cpan -i Moo
# Execute in Cygwin console:
# > perl french_flex.pl --models=/path/to/models.txt --lemmas=/path/to/lemmas.txt

binmode(STDOUT, ":utf8"); # Force utf-8 output
binmode(STDERR, ":utf8"); # Force utf-8 output

# Sorted list of attributes of the rules that must be retrieved in every form
my @MODEL_FEATURES_TO_RETRIEVE = qw(tense person number mood gender);

####################
##### PACKAGES #####
####################

# This class defines a rule in a model
# Every rule has an id and a set of key/value pairs

package Rule
{
    use Moo;
    has id => ( is  => 'ro', isa => sub { die "Invalid type for attribute id" 
					      unless $_[0] =~ /^[0-9]+$/; }, required => 1);

    has features => ( is  => 'ro', isa => sub { die "Invalid type for attribute features" 
						    unless ref($_[0]) eq 'HASH'; }, required => 1);
    sub get
    {
	my ($self,$key) = @_;

	die "Unavailable feature $key in rule" 
	    unless exists $self->features->{$key};
	$self->features->{$key};
    }
}

# This class defines a model. Avery model has an id and a set of rules

package Model
{
    use Moo;
    has id => ( is  => 'ro', isa => sub { die "Invalid type for attribute id" 
					      unless $_[0] =~ /^[0-9]+$/; }, required => 1);

    has rules => ( is  => 'ro', isa => sub { die "Invalid type for attribute features" 
						 unless ref($_[0]) eq 'HASH'; }, required => 1);

    sub add_rule
    {
	my ($self,$rule) = @_;
	$self->rules->{$rule->id} = $rule;
    }

    sub get_rule
    {
	my ($self,$id) = @_;
	die "Unavailable rule $id in model" 
	    unless exists $self->rules->{$id};
	$self->rules->{$id};
    }
}

# This class defines a lemma to be inflected

package Lemma
{
    use Moo;

    has features => ( is  => 'ro', isa => sub { die "Invalid type for attribute features" 
						    unless ref($_[0]) eq 'HASH'; }, required => 1);
    sub get
    {
	my ($self,$key) = @_;
	die "Unavailable feature $key in lemma" 
	    unless exists $self->features->{$key};
	$self->features->{$key};
    }
}

################################
##### COMMAND LINE OPTIONS #####
################################

my $models_file;
my $lemmas_file;

GetOptions ("models=s" => \$models_file,
	    "lemmas=s" => \$lemmas_file) or die("Error in command line arguments\n");

die "No input files provided" unless $models_file && $lemmas_file;

################
##### MAIN #####
################

#################################
### READ MODELS RULES TO FLEX ###
#################################

open my $models_file_fh, '<:encoding(UTF-8)', $models_file 
    || die "Models file $models_file not found";
my @models_attributes; # To store the header of the file

# Repository with all the models
my $MODELS;

# Store in $MODELS a model rule for every line in the
# models file

while (<$models_file_fh>)
{
    chomp;
    # The first line in the file has the names of the fields
    unless (@models_attributes) { @models_attributes = m/([^\t]+)/g; next;} 
    # For the rest of lines...
    my @values = m/([^\t]+)/g;
    my $features;
    $features->{$models_attributes[$_]} = $values[$_] for  (0 .. $#models_attributes);
    my $model_id = delete $features->{model} // die "No model field in models file row";
    my $rule_id = delete $features->{rule} // die "No rule field in models file row";
    my $rule = Rule->new(id => $rule_id, features => $features);

    $MODELS->{$model_id} //= Model->new(id => $model_id, rules => {});
    $MODELS->{$model_id}->add_rule($rule);
}

close $models_file_fh;

#######################
##### FLEX LEMMAS #####
#######################

open my $lemmas_file_fh, '<:encoding(UTF-8)', $lemmas_file 
    || die "Lemmas file $models_file not found";
my @lemmas_attributes; # To store the header of the file

# For every lemma (line) in the file, that lemma is flexed
# with the flex() function
while (<$lemmas_file_fh>)
{
    chomp;
    next unless /\S+/;
    # The first line in the file has the names of the fields
    unless (@lemmas_attributes) { @lemmas_attributes = m/([^\t]+)/g; next;} 
    # For the rest of lines...
    my @values = m/([^\t]+)/g;
    my $features;
    $features->{$lemmas_attributes[$_]} = $values[$_] for  (0 .. $#lemmas_attributes);

    my $lemma = Lemma->new(features => $features);
    
    flex($lemma);
}

close $lemmas_file_fh;

#################
### FUNCTIONS ###
#################


################################
# Main function to flex lemmas #
################################

sub flex
{
    my $lemma = shift;
    my $model = $MODELS->{$lemma->get('model')};
 
    unless ($model)
    {
	say STDERR "No model for lemma ". $lemma->get('lemma');
	return;
    }

    my $rules = $model->rules;

    for my $rule (values %$rules)
    {
	my $form;

	# Is a special rule?
	if (my $n = $rule->get('special'))
	{
	    my $f = 'apply_special_rule_'.$n;
	    no strict; # Needed to execute the function this way
	    $form = $f->($lemma,$rule);
	}
	else
	{
	    $form = apply_default_rule($lemma,$rule); 
	}
	print_form($form,$lemma,$rule); # Print a line in STDOUT
    }
}


#####################################
# Auxiliar functions to flex lemmas #
#####################################

###################
# The default one #
###################

sub apply_default_rule
{
    my ($lemma,$rule) = @_;
    my $form = $lemma->get('lemma');

    my $to_remove = $rule->get('remove') || ''; # 0 means no remove needed
    my $to_add = $rule->get('add') || ''; # 0 means no addition needed
    $to_add =~ s/\\/\$/;
# http://stackoverflow.com/questions/25309798/using-string-variables-containing-literal-escapes-in-a-perl-substitution   
    $form =~ s/$to_remove$/qq{"$to_add"}/ee;

    return $form;
}

#################################
# Functions for 3 special cases #
#################################
# Nothing for French

sub apply_special_rule_1 { }

sub apply_special_rule_2 { }

sub apply_special_rule_3 { }

########################################
# Function to print a form via console #
########################################

sub print_form
{
    my ($form,$lemma_obj,$rule) = @_;
    my %features;
    
    $features{$_} = $rule->get($_) for @MODEL_FEATURES_TO_RETRIEVE; 
    my $lemma = $lemma_obj->get('lemma');
    my $POS = $lemma_obj->get('POS');

    my @rest_of_attributes = map { $features{$_} } @MODEL_FEATURES_TO_RETRIEVE;	

    my $out = "$form\t$lemma\t$POS";
    $out .= "\t$_" for @rest_of_attributes;
    say $out;
}


#
#
#This README file describes the structure and content  of the French Lexical Data for the inflection models and rules of the Data.
#
#This data is included in the file "Bitext_FR_ModelRules.txt". It's a plain text file and uses UTF-8 encoding. 
#
#FILE STRUCTURE
#	The file has the following structure: 
#		-First line. The first line is the header that describes the contents of every field. 
#		-Rest of lines. The rest of the file contains one inflection rule per line (described below).
#
#	The fields are separated by the TAB character.
#
#FILE CONTENTS
#	This file contains 
#		-The list of inflection rules used to obtain the full inflection of the lemmas present in the file "Bitext_FR_LemmaModels.txt", pertaining to Noun, Adjective and Verb POS.
#		-The attributes of the forms generated with these inflection rules.
#
#	Fields and attributes:
#		-model: the inflection model
#		-rule: the number of the rule described in the line
#		-remove: the part of the lemma that has to be removed from the end of the word
#		-add: the string that has to be attached to the end of the word to obtain the inflected form
#		-special: "1" means that a special rule has to be applied, described below. "0" means no special rule has to be applied. For French there is no special rules to be applied, so the value of this field is always "0"
#		-attributes:
#			-tense: present, imperfect, past, future, conditional, infinitive, past participle, present participle, only for Verb POS
#			-person: 1, 2, 3, only for Verb POS 
#			-number: singular, plural, for all POS
#			-mood: indicative, subjunctive, imperative, for Verb POS
#			-gender: masculine, femenine, neuter, for all POS
#
#FILE INSTRUCTIONS
#	Steps to obtain the full inflection of a lemma from this file:
#		-Get the inflection model of the choosen lemma from the file "Bitext_FR_LemmaModels.txt"
#		-Start applying all the inflection rules in this file that have that model in the column "model".
#		-For each line, apply the rules as follows:
#			-remove from the end of the lemma the string appearing in the field "remove" (example: from "aimer", remove "er").
#				-If the field contains "0" nothing has to be removed
#			-add to the end of the resulting form the string appearing in the field "remove" (example: from "aim", add "ons")
#				-If the field contains "0" nothing has to be added
#
#
#			
#
#	
#	
#
#	

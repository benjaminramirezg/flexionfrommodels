#/usr/lib/perl
use strict;
use warnings;
use 5.010;
use Getopt::Long;

# This script takes a Model Rules File for German
# and a list of Lemmas for German.

# It parses the Model Rules and store a bunch of
# models with rules to flex.

# With that models stored, the file of lemmas is read
# For every lemma, according the stored model correspondig
# that lemma, the flexed forms are generated and printed in STDOUT. 

# At the end of the file appears the README that describes the files
# that must be provided to this script

##############
### README ###
##############

# Perl 14.04 needed
# Module Moo needed. In order to install it:
# > cpan -i Moo
# Execute in Cygwin console:
# > perl german_flex.pl --models=/path/to/models.txt --lemmas=/path/to/lemmas.txt

binmode(STDOUT, ":utf8"); # Force utf-8 output

# Sorted list of attributes of the rules that must be retrieved in every form
my @MODEL_FEATURES_TO_RETRIEVE = qw(tense mood person number gender case declension degree);

####################
##### PACKAGES #####
####################

# This class defines a rule in a model
# Every rule has an id and a set of key/value pairs

package Rule
{
    use Moo;
    has id => ( is  => 'ro', isa => sub { die "Invalid type for attribute id" 
					      unless $_[0] =~ /^[0-9]+$/; }, required => 1);

    has features => ( is  => 'ro', isa => sub { die "Invalid type for attribute features" 
						    unless ref($_[0]) eq 'HASH'; }, required => 1);
    sub get
    {
	my ($self,$key) = @_;
	die "Unavailable feature $key in rule" 
	    unless exists $self->features->{$key};
	$self->features->{$key};
    }
}

# This class defines a model. Avery model has an id and a set of rules

package Model
{
    use Moo;
    has id => ( is  => 'ro', isa => sub { die "Invalid type for attribute id" 
					      unless $_[0] =~ /^[0-9]+$/; }, required => 1);

    has rules => ( is  => 'ro', isa => sub { die "Invalid type for attribute features" 
						 unless ref($_[0]) eq 'HASH'; }, required => 1);

    sub add_rule
    {
	my ($self,$rule) = @_;
	$self->rules->{$rule->id} = $rule;
    }

    sub get_rule
    {
	my ($self,$id) = @_;
	die "Unavailable rule $id in model" 
	    unless exists $self->rules->{$id};
	$self->rules->{$id};
    }
}

# This class defines a lemma to be inflected

package Lemma
{
    use Moo;

    has features => ( is  => 'ro', isa => sub { die "Invalid type for attribute features" 
						    unless ref($_[0]) eq 'HASH'; }, required => 1);
    sub get
    {
	my ($self,$key) = @_;
	die "Unavailable feature $key in lemma" 
	    unless exists $self->features->{$key};
	$self->features->{$key};
    }
}

################################
##### COMMAND LINE OPTIONS #####
################################

my $models_file;
my $lemmas_file;

GetOptions ("models=s" => \$models_file,
	    "lemmas=s" => \$lemmas_file) or die("Error in command line arguments\n");

die "No input files provided" unless $models_file && $lemmas_file;

################
##### MAIN #####
################

#################################
### READ MODELS RULES TO FLEX ###
#################################

open my $models_file_fh, '<:encoding(UTF-8)', $models_file 
    || die "Models file $models_file not found";
my @models_attributes; # To store the header of the file

# Repository with all the models
my $MODELS;

# Store in $MODELS a model rule for every line in the
# models file

while (<$models_file_fh>)
{
    chomp;

    next unless /\S+/;
    # The first line in the file has the names of the fields
    unless (@models_attributes) { @models_attributes = m/([^\t]+)/g; next;} 
    # For the rest of lines...
    my @values = m/([^\t]+)/g;
    my $features;
    $features->{$models_attributes[$_]} = $values[$_] for  (0 .. $#models_attributes);
    my $model_id = delete $features->{model} // die "No model field in models file row";
    my $rule_id = delete $features->{rule} // die "No rule field in models file row";
    my $rule = Rule->new(id => $rule_id, features => $features);

    $MODELS->{$model_id} //= Model->new(id => $model_id, rules => {});
    $MODELS->{$model_id}->add_rule($rule);
}

close $models_file_fh;

#######################
##### FLEX LEMMAS #####
#######################

open my $lemmas_file_fh, '<:encoding(UTF-8)', $lemmas_file 
    || die "Lemmas file $models_file not found";
my @lemmas_attributes; # To store the header of the file


# For every lemma (line) in the file, that lemma is flexed
# with the flex() function
while (<$lemmas_file_fh>)
{
    chomp;
    next unless /\S+/;
    # The first line in the file has the names of the fields
    unless (@lemmas_attributes) { @lemmas_attributes = m/([^\t]+)/g; next;} 
    # For the rest of lines...
    my @values = m/([^\t]+)/g;

    my $features;
    $features->{$lemmas_attributes[$_]} = $values[$_] for  (0 .. $#lemmas_attributes);
    my $lemma = Lemma->new(features => $features);
    
    flex($lemma);
}

close $lemmas_file_fh;

#################
### FUNCTIONS ###
#################

# Main function to flex lemmas

sub flex
{
    my $lemma = shift;
    my $model = $MODELS->{$lemma->get('model')};

    unless ($model)
    {
        say STDERR "No model for lemma ". $lemma->get('lemma');
        return;
    }

    my $rules = $model->rules;

    for my $rule (values %$rules)
    {
	my $form;

	# Is a special rule?
	if (my $n = $rule->get('special'))
	{
	    my $f = 'apply_special_rule_'.$n;
	    no strict; # Needed to execute the function this way
	    $form = $f->($lemma,$rule);	    
	}
	else
	{
	    $form = apply_default_rule($lemma,$rule); 
	}
	print_form($form,$lemma,$rule); # Print a line in STDOUT
    }
}

#####################################
# Auxiliar functions to flex lemmas #
#####################################

###################
# The default one #
###################

sub apply_default_rule
{
    my ($lemma,$rule) = @_;
    my $form = $lemma->get('lemma');

    my $to_remove = $rule->get('remove') || ''; # 0 means no remove needed
    my $to_add = $rule->get('add') || ''; # 0 means no addition needed
    $to_add =~ s/\\/\$/;
# http://stackoverflow.com/questions/25309798/using-string-variables-containing-literal-escapes-in-a-perl-substitution   
    $form =~ s/$to_remove$/qq{"$to_add"}/ee;

    return $form;
}

#################################
# Functions for 3 special cases #
#################################

sub apply_special_rule_1
{
    my ($lemma,$rule) = @_;
    my $form = apply_default_rule($lemma,$rule);
    my $repposition = $lemma->get('repposition') || return $form;
    my $repchar = $lemma->get('repchar');

    $repposition--;
    substr($form,$repposition,1) = $repchar;

    return $form ;
}

sub apply_special_rule_2
{
    my ($lemma,$rule) = @_;
    my $form = apply_default_rule($lemma,$rule);

    my $flags = $lemma->get('flags');
    return $form unless $flags =~ /G/;
    
    my $insposition = $lemma->get('insposition');
    my $insertstring = $rule->get('insertstring');

    substr($form,$insposition,0) = $insertstring;

    
    return $form;
}

sub apply_special_rule_3
{
    my ($lemma,$rule) = @_;
    my $form = apply_default_rule($lemma,$rule);

    my $flags = $lemma->get('flags');
    return $form unless $flags =~ /Z/;

    my $insposition = $lemma->get('insposition');
    my $insertstring = $rule->get('insertstring');

    substr($form,$insposition,0) = $insertstring;

    return $form;
}

########################################
# Function to print a form via console #
########################################

sub print_form
{
    my ($form,$lemma_obj,$rule) = @_;
    my %features;
    
    $features{$_} = $rule->get($_) for @MODEL_FEATURES_TO_RETRIEVE; 
    my $lemma = $lemma_obj->get('lemma');
    my $POS = $lemma_obj->get('POS');
    
    # FIX ME: This is for German. I'd like to do this in other place
    $features{gender} = $lemma_obj->get('gender')
	if $lemma_obj->get('POS') eq 'noun';   

    my @rest_of_attributes = map { $features{$_} } @MODEL_FEATURES_TO_RETRIEVE;	

    my $out = "$form\t$lemma\t$POS";
    $out .= "\t$_" for @rest_of_attributes;
    say $out;
}

#
#This README file describes the structure and content  of the German Lexical Data for the inflection models and rules of the Data.
#
#This data is included in the file "Bitext_DE_ModelRules.txt". It's a plain text file and uses UTF-8 encoding. 
#
#FILE STRUCTURE
#	The file has the following structure: 
#		-First line. The first line is the header that describes the contents of every field. 
#		-Rest of lines. The rest of the file contains one inflection rule per line (described below).
#
#	The fields are separated by the TAB character.
#
#FILE CONTENTS
#	This file contains 
#		-The list of inflection rules used to obtain the full inflection of the lemmas present in the file "Bitext_DE_LemmaModels.txt", pertaining to Noun, Adjective and Verb POS.
#		-The attributes of the forms generated with these inflection rules.
#
#	Fields and attributes:
#		-model: the inflection model
#		-rule: the number of the rule described in the line
#		-remove: the part of the lemma that has to be removed from the end of the word
#		-add: the string that has to be attached to the end of the word to obtain the inflected form
#		-special: "1" means that a special rule has to be applied, described below. "0" means no special rule has to be applied. For French there is no special rules to be applied, so the value of this field is always "0"
#		-insertstring: the string to be inserted in the generation of certain inflected forms
#		-attributes:
#			-tense: XXXXXXXXXXXXXX present, imperfect, past, future, conditional, infinitive, past participle, present participle, only for Verb POS
#			-mood: indicative, subjunctive, imperative, infinitive, present participle, past participle, only for Verb POS
#			-person: 1, 2, 3, only for Verb POS 
#			-number: singular, plural, for all POS
#			-gender: masculine, feminine, neuter, for all POS
#			-case: nominative, accusative, genitive, dative (with variants), for Noun and Adj POS
#			-declension: strong, weak, mixed, only for Adj POS
#			-degree: positive, comparative, superlative, only for Adj POS
#
#FILE INSTRUCTIONS
#	Instructions to obtain the full inflection of a lemma from this file are described below. The instructions have the following parts:
#
#	1) OVERVIEW OF TABLES
#	2) DEFAULT INFLECTION ALGORITHM
#	3) SPECIAL FEATURE (NOUN GENDER ASSIGNEMENT)
#	4) SPECIAL RULES
#	
#=================
#
#	OVERVIEW OF TABLES
#	
#	Two tables are provided for inflection generation: "Bitext_DE_LemmaModels.txt" ("LemmaModel") and "Bitext_DE_ModelRules.txt" ("ModelRule").
#
#	LemmaModel table:
#		Includes the lemmas, POS and some parameters necessary for the inflection.
#		Details of the meaning of each one are provided below:
#			field1, "lemma" is the base lemma that will be used for the inflection. It normally corresponds to the nominative singular of nouns (v.g. Buch), the positive base form of adjectives (v.g. "abominabel") or the standard infinitive of verbs (v.g. "decken")
#			field2, "POS" indicates the Part of Speech of the lemma. It should be one of "noun", "adjective" of "verb".
#			field3, "model" is the model ID that must be used to cross this table appropriately with the ModelRule table
#			field4 "subcat" indicates the verb subcategorization ("T" for Transitive, "I" for Intransitive, "R" for Reflexive)
#			field5 "insposition" indicates the insertion position to be used in case of application of special rules 2 and 3 (see below).
#			field6 "flags" indicates if special rules 2 and 3 have to be applied to this particular verb lemma (see below).
#			field7 "repposition" indicates the replacement position to be used in case of application of special rule 1 (see below).
#			field8 "repchar" indicates the replacement character to be used in case of application of special rule 1 (see below).
#			field9 "gender" indicates the value to be used as value for the gender morphological attribute in the noun inflection (more details below).
#
#		LemmaModel table sample:
#			lemma	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#			aalen	verb	2001	R	0	G	n/a	n/a	n/a
#			Buch	noun	1070	n/a	n/a	n/a	2	ü	neuter
#
#
#	ModelRule table:
#		Includes the full inflection models with the inflection rules plus the corresponding morphological attributes.
#		Details of the meaning of each one are provided below:
#			field1, "model" is the model ID that must be used to cross this table appropriately with the LemmaModel table
#			field2, "rule" is a sequential ID for the rules corresponding to the same "model". (This field can be ignored for most purposes)
#			field3 of ModelRule table ("remove") indicates the rightmost part of the lemma to be deleted, in order to obtain the form. A value of 0 indicates that nothing is to be removed.
#			field4 of ModelRule table ("add") indicates the text to be added to the remaining part of the lemma after the rightmost part of the lemma has been deleted according to field 3 above.  A value of 0 indicates that nothing is to be added.
#			field5 of ModelRule table ("special") indicates if a special operation is required
#			field6 might contain additional data for the special operation (text to be inserted)
#
#		Last 8 fields of the ModelRule table are the values of the morphological attributes (tense, mood, person, number, gender, case, declension, degree).
#
#		ModelRule table sample:
#			model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	
#			1070	001	0	0	0	0	n/a	n/a	n/a	singular	*	nominative	n/a	n/a
#			1070	002	0	es	0	0	n/a	n/a	n/a	singular	*	genitive	n/a	n/a
#			1070	003	0	s	0	0	n/a	n/a	n/a	singular	*	genitive	n/a	n/a
#			1070	004	0	er	1	0	n/a	n/a	n/a	plural	*	nominative	n/a	n/a
#			1070	005	0	er	1	0	n/a	n/a	n/a	plural	*	accusative	n/a	n/a
#			1070	006	0	er	1	0	n/a	n/a	n/a	plural	*	genitive	n/a	n/a
#			1070	007	0	ern	1	0	n/a	n/a	n/a	plural	*	dative	n/a	n/a
#			1070	008	0	0	0	0	n/a	n/a	n/a	singular	*	accusative	n/a	n/a
#			1070	009	0	0	0	0	n/a	n/a	n/a	singular	*	dative	n/a	n/a
#			...
#			2001	001	0	0	0	0	n/a	infinitive	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	003	en	0	0	0	present	imperative	2	singular	n/a	n/a	n/a	n/a
#			2001	004	en	e	0	0	present	indicative	1	singular	n/a	n/a	n/a	n/a
#			2001	005	en	e	0	0	present	subjunctive	1	singular	n/a	n/a	n/a	n/a
#			2001	006	en	e	0	0	present	subjunctive	3	singular	n/a	n/a	n/a	n/a
#			2001	007	en	e	0	0	present	imperative	2	singular	n/a	n/a	n/a	n/a
#			2001	008	en	st	0	0	present	indicative	2	singular	n/a	n/a	n/a	n/a
#			2001	009	en	t	0	0	present	indicative	2	plural	n/a	n/a	n/a	n/a
#			2001	010	en	t	0	0	present	indicative	3	singular	n/a	n/a	n/a	n/a
#			2001	011	en	t	0	0	present	imperative	2	plural	n/a	n/a	n/a	n/a
#			2001	012	n	st	0	0	present	subjunctive	2	singular	n/a	n/a	n/a	n/a
#			2001	013	n	t	0	0	present	subjunctive	2	plural	n/a	n/a	n/a	n/a
#			2001	014	en	te	0	0	past	indicative	1	singular	n/a	n/a	n/a	n/a
#			2001	015	en	te	0	0	past	indicative	3	singular	n/a	n/a	n/a	n/a
#			2001	016	en	te	0	0	past	subjunctive	1	singular	n/a	n/a	n/a	n/a
#			2001	017	en	te	0	0	past	subjunctive	3	singular	n/a	n/a	n/a	n/a
#			2001	018	en	test	0	0	past	indicative	2	singular	n/a	n/a	n/a	n/a
#			2001	019	en	test	0	0	past	subjunctive	2	singular	n/a	n/a	n/a	n/a
#			2001	020	en	ten	0	0	past	indicative	1	plural	n/a	n/a	n/a	n/a
#			2001	021	en	ten	0	0	past	subjunctive	3	plural	n/a	n/a	n/a	n/a
#			2001	022	en	ten	0	0	past	indicative	3	plural	n/a	n/a	n/a	n/a
#			2001	023	en	ten	0	0	past	subjunctive	1	plural	n/a	n/a	n/a	n/a
#			2001	024	en	tet	0	0	past	indicative	2	plural	n/a	n/a	n/a	n/a
#			2001	025	en	tet	0	0	past	subjunctive	2	plural	n/a	n/a	n/a	n/a
#			2001	026	0	d	0	0	n/a	present participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	027	0	de	0	0	n/a	present participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	028	0	der	0	0	n/a	present participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	029	0	den	0	0	n/a	present participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	030	0	dem	0	0	n/a	present participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	031	0	des	0	0	n/a	present participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	032	en	t	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	033	en	te	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	034	en	ter	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	035	en	ten	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	036	en	tem	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#			2001	037	0	0	0	0	present	imperative	3	plural	n/a	n/a	n/a	n/a
#			2001	038	0	0	0	0	present	indicative	1	plural	n/a	n/a	n/a	n/a
#			2001	039	0	0	0	0	present	indicative	3	plural	n/a	n/a	n/a	n/a
#			2001	040	0	0	0	0	present	subjunctive	3	plural	n/a	n/a	n/a	n/a
#			2001	041	0	0	0	0	present	imperative	1	plural	n/a	n/a	n/a	n/a
#			2001	042	0	0	0	0	present	subjunctive	1	plural	n/a	n/a	n/a	n/a
#
#		Full inflection is achieved by crossing the entries of the LemmaModel table with the entries of the ModelRule table which share the same "model" ID.
#
#=================
#
#	DEFAULT INFLECTION ALGORITHM:
#
#		For all inflection rules, two operations apply by default. Inflection is generated by combining an entry of the "LemmaModel" table with an entry of the "ModelRule" table. They both should share the same "model" value.
#
#	Default operations algorithm is as follows:
#		Step 1: Remove the text indicated by the "remove" field in the "ModelRule" entry from the rightmost part of the corresponding "lemma" taken from the "LemmaModel" entry. (A value of "0" in the "remove" field indicates that nothing is to be removed)
#		Step 2: Add the text indicated by the "add" field in the "ModelRule" entry to the resulting string of Step 1 above. ("0" indicates that nothing is to be added)
#
#		Example:
#			LemmaModel entry
#				lemma	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#				aalen	verb	2001	R	0	G	n/a	n/a	n/a
#
#			ModelRule entry
#				model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#				2001	004	en	e	0	0	present	indicative	1	singular	n/a	n/a	n/a	n/a
#
#			Note that "model" value is 2001 in both entries.
#
#			Step 1: Remove "en"
#				aalen ==> aal				
#			Step 2: Add "e"
#				aal ==> aale
#
#			Inflection result:
#				form	lemma	POS	tense	mood	person	number	gender	case	declension	degree
#				aale	aalen	verb	present	indicative	1	singular	n/a	n/a	n/a	n/a
#
#=================
#
#	SPECIAL FEATURE (NOUN GENDER ASSIGNEMENT):
#
#	In German, in all Noun models, the value for the gender attribute must be assigned according to the value of the "gender" field (last field) of the corresponding LemmaModel Entry.
#	In the "ModelRule" entry the place for the gender attribute will be marked with "*" (an asterisk).
#
#	Example:
#		lema	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#		Buch	noun	1070	n/a	n/a	n/a	2	ü	neuter
#
#		ModelRule Entry
#			model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#			1070	007	0	ern	1	0	n/a	n/a	n/a	plural	*	dative	n/a	n/a
#
#		For the inflection, in the attributes string present in the ModelRule Entry:
#			n/a	n/a	n/a	plural	*	dative	n/a	n/a
#		the "*" has to be replaced by the gender indicated by the value of the "gender" field of the LemmaModel entry, giving the following result:
#			n/a	n/a	n/a	plural	neuter	dative	n/a	n/a
#
#=================
#
#	SPECIAL RULES
#	
#	The field5 ("special") in ModelRule entries indicates if a special rule must be applied. 
#	
#	A value of "0" indicates that no special rule must be applied, and only the default rules have to be applied (plus the gender assignment if the lemma is a noun).
#
#	In German there are 3 special rules marked by the values "1", "2", and "3" respectively.
#
#	
#	SPECIAL RULE "1"
#
#		Rule "1": Diaeresis addition in nouns/adjectives. Field5 value in ModelRule ("special") is "1".
#
#		In order to apply this special rule the "LemmaModel" entry fields "repposition" and "repchar" must be taken into account.
#
#		Algorithm:
#			1. Apply default operations "remove"
#			2. Apply default operation "add"
#			3. If "repposition" field of the LemmaModel Entry is greater than 0, the special rule 1 HAS TO be applied. 
#				If "repposition" value is 0, the inflection is valid "as is" at this step. Nothing else has to be made.
#			4. At the position marked by the "repposition" field of the LemmaModel Entry, REPLACE the existing character by the character indicated by the "repchar" field of the ModelRule Entry
#			5. If the POS is "noun" take the gender from the "gender" field of the LemmaModel entry. If POS is "adjective", we are done.
#
#		Example 1:
#			LemmaModel Entry
#				lema	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#				Buch	noun	1070	n/a	n/a	n/a	2	ü	neuter
#
#			ModelRule Entry
#				model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#				1070	007	0	ern	1	0	n/a	n/a	n/a	plural	*	dative	n/a	n/a
#
#			Step 1: Remove 0 (=nothing)
#				Buch => Buch
#			Step 2: Add "ern"
#				Buch => Buchern
#			Step 3: "repposition" value is "2", so rule 1 HAS TO be applied.
#			Step 4: replace character at position 2 (as indicated by "repposition") by "ü" (as indicated by "repchar")
#				Buchern => Büchern
#			Step 5: "POS" value in LemmaModel is "noun" The gender field (marked with "*") must be assigned the value "neuter" (as indicated by "gender")
#
#			Inflection result:
#				Büchern	Buch	noun	n/a	n/a	n/a	plural	neuter	dative	n/a	n/a
#
#	
#	SPECIAL RULE "2"
#
#		Rule "2": Past participle must "insert" "ge/geg". Applies only to verbs. Field5 value in ModelRule ("special") is "2". Field6 value in ModelRule ("insertstring") indicates the text to be inserted (if any). This is normally "ge" (and "geg" for a few verbs).
#
#		In order to apply this special rule the "LemmaModel" entry fields "insposition" and "flags" must be taken into account.
#		
#		Algorithm:
#			1. Apply default operations "remove"
#			2. Apply default operation "add"
#			3. If "flags" field of the LemmaModel Entry contains the flag "G", the special rule 2 HAS TO be applied. 
#				If "G" is not present in "flags", the inflection is valid "as is" at this step. Nothing else has to be made.
#			4. At the position marked by the "insposition" field of the LemmaModel Entry, INSERT the text indicated by the "insertstring" field of the ModelRule Entry. If the "insposition" alue is 0, insert the text to the left of the from being composed.
#
#		Example 1:
#			LemmaModel Entry
#				lema	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#				abdecken	verb	2111	T	2	GZ	n/a	n/a	n/a
#
#			ModelRule Entry
#				model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#				2111	029	en	t	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#
#			Step 1: Remove "en"
#				abdecken => abdeck
#			Step 2: Add "t"
#				abdeck => abdeckt
#			Step 3: "G" is present in "flags" at "LemmaModel", so rule 2 HAS TO be applied.
#			Step 4: Insert text. At position "2" (as indicated by "insposition" in LemmaModel), "ge" (as indicated by "insertstring" in ModelRule) HAS TO BE INSERTED
#				abdeckt => abgedeckt
#
#			Inflection result:
#				abgedeckt	abdecken	verb	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#
#		Example 2:
#			LemmaModel Entry
#				lema	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#				anbefehlen	verb	2144	T	2	Z	n/a	n/a	n/a
#
#			ModelRule Entry
#				model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#				2144	036	ehlen	ohlen	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#
#			Step 1: Remove "ehlen"
#				anbefehlen => anbef
#			Step 2: Add "ohlen"
#				anbef => anbefohlen
#			Step 3: "G" is NOT present in "flags" at "LemmaModel", so rule 2 DOES NOT HAVE TO be applied.
#
#			Inflection result:
#				anbefohlen	anbefehlen	verb	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#
#		Example 3:
#			LemmaModel Entry
#				lema	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#				decken	verb	2001	TR	0	G	n/a	n/a	n/a
#
#			ModelRule Entry
#				model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#				2001	032	en	t	2	ge	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#
#			Step 1: Remove "en"
#				decken => deck
#			Step 2: Add "t"
#				deck => deckt
#			Step 3: "G" is present in "flags" at "LemmaModel", so rule 2 HAS TO be applied.
#			Step 4: Insert text. At position "0" (= to the very left) (as indicated by "insposition" in LemmaModel), "ge" (as indicated by "insertstring" in ModelRule) HAS TO BE INSERTED
#				deckt => gedeckt
#
#			Inflection result:
#				gedeckt	decken	verb	n/a	past participle	n/a	n/a	n/a	n/a	n/a	n/a
#
#				
#	SPECIAL RULE "3"
#
#	Rule "3": Additional infinitive exist by adding "zu". Applies only to verbs. Field5 value in ModelRule ("special") is "3". Field6 value in ModelRule ("insertstring") indicates the text to be inserted (if any). This is "zu" for all verbs.
#
#	In order to apply this special rule the "LemmaModel" entry fields "insposition" and "flags" must be taken into account.
#
#	Algorithm:
#		1. If "flags" field of the LemmaModel Entry contains the flag "Z", the special rule 3 HAS TO be applied. 
#			If "Z" would be not present in "flags", this rule does not produce any valid inflection.
#		2. Apply default operations "remove"
#		3. Apply default operation "add"
#		4. At the position marked by the "insposition" field of the LemmaModel Entry, INSERT the text indicated by the "insertstring" field of the ModelRule Entry. If the "insposition" alue is 0, insert the text to the left of the from being composed.
#
#	Example 1:
#		LemmaModel Entry
#			lema	POS	 model	subcat	insposition	flags	repposition	repchar	gender
#			abdecken	verb	2111	T	2	GZ	n/a	n/a	n/a
#
#		ModelRule Entry
#			model	rule	remove	add	special	insertstring	tense	mood	person	number	gender	case	declension	degree
#			2111	002	0	0	3	zu	n/a	infinitive	n/a	n/a	n/a	n/a	n/a	n/a
#
#		Step 1: "Z" is present in "flags" at "LemmaModel", so rule 3 HAS TO be applied.
#		Step 2: Remove 0 (=nothing)
#			abdecken => abdecken
#		Step 3: Add 0 (=nothing)
#			abdecken => abdecken
#		Step 4: Insert text. At position "2" (as indicated by "insposition" in LemmaModel), "zu" (as indicated by "insertstring" in ModelRule) HAS TO BE INSERTED
#			abdecken => abzudecken
#
#		Inflection result:
#			abzudecken	abdecken	verb	n/a	infinitive	n/a	n/a	n/a	n/a	n/a	n/a
#
#
#	
